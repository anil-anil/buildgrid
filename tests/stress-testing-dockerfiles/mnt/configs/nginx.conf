# NOTE: This file is a slightly adapted version of https://github.com/grpc/grpc-web/blob/master/net/grpc/gateway/examples/echo/nginx.conf
master_process off;
worker_processes auto;
pid nginx.pid;
error_log stderr debug;

events {
  worker_connections 1024;
}

http {
  access_log off;
  client_max_body_size 0;
  client_body_temp_path client_body_temp;
  proxy_temp_path proxy_temp;
  proxy_request_buffering off;

  server {
    # Doing the grpc_pass this way makes nginx
    # do a DNS lookup and respect the TTL
    # allowing it to pick up changes in the
    # available buildgrid containers
    # in the docker-compose
    # (e.g. when there are more/fewer/different 
    #  available hosts due to `--scale` changes
    #  at runtime)
    resolver 127.0.0.11 valid=1s; # Embedded Docker DNS resolver
    set $buildgrid_endpoint grpc://buildgrid:50051;

    listen 50051 http2;
    server_name localhost;
    location / {
      grpc_pass $buildgrid_endpoint;
      grpc_read_timeout 120s;
      grpc_send_timeout 120s;
      grpc_connect_timeout 120s;
      grpc_next_upstream error;
      if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Transfer-Encoding,Custom-Header-1,X-Accept-Content-Transfer-Encoding,X-Accept-Response-Streaming,X-User-Agent,X-Grpc-Web';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
      }
      if ($request_method = 'POST') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Transfer-Encoding,Custom-Header-1,X-Accept-Content-Transfer-Encoding,X-Accept-Response-Streaming,X-User-Agent,X-Grpc-Web';
        add_header 'Access-Control-Expose-Headers' 'Content-Transfer-Encoding';
      }
    }
  }
}
