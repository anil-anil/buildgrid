# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from concurrent import futures
from contextlib import contextmanager
import multiprocessing
import os
import signal

import grpc
import pytest_cov

from buildgrid.server.logstream.instance import LogStreamInstance
from buildgrid.server.logstream.service import LogStreamService
from buildgrid.server.logstream.streaming.memory import StreamStorage



@contextmanager
def serve_logstream(instances):
    server = TestServer(instances)
    try:
        yield server
    finally:
        server.quit()


class TestServer:

    def __init__(self, instances):
        self.instances = instances

        self._queue = multiprocessing.Queue()
        self._process = multiprocessing.Process(
            target=TestServer.serve,
            args=(self._queue, self.instances))
        self._process.start()

        self.port = self._queue.get()
        self.remote = f'localhost:{self.port}'

    @classmethod
    def serve(cls, queue, instances):
        pytest_cov.embed.cleanup_on_sigterm()

        # Use max_workers default from Python 3.4+
        max_workers = (os.cpu_count() or 1) * 5
        server = grpc.server(futures.ThreadPoolExecutor(max_workers))
        port = server.add_insecure_port('localhost:0')

        logstream_service = LogStreamService(server)
        for name in instances:
            logstream_instance = LogStreamInstance('', StreamStorage())
            logstream_service.add_instance(name, logstream_instance)

        server.start()
        queue.put(port)

        signal.pause()

    def quit(self):
        if self._process:
            self._process.terminate()
            self._process.join()
