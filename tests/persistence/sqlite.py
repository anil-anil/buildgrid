# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


from datetime import datetime, timedelta
from time import sleep
import os
import tempfile
import hashlib
import json

import pytest
from unittest import mock
from sqlalchemy.pool.impl import StaticPool

from buildgrid._enums import LeaseState, MetricCategories, OperationStage
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.rpc import code_pb2, status_pb2
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.job import Job
from buildgrid.server.operations.filtering import FilterParser, DEFAULT_OPERATION_FILTERS
from buildgrid.server.persistence.sql import models
from buildgrid.server.persistence.sql.impl import SQLDataStore
from buildgrid.server.persistence.sql.utils import DATETIME_FORMAT
from buildgrid.utils import hash_from_dict
from buildgrid._exceptions import DatabaseError, InvalidArgumentError

from google.protobuf.timestamp_pb2 import Timestamp
from google.protobuf import any_pb2


@pytest.fixture()
def database():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    try:
        db = tempfile.NamedTemporaryFile().name
        data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
        yield data_store

    finally:
        data_store.watcher_keep_running = False


@pytest.fixture()
def database_pair():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    try:
        db = tempfile.NamedTemporaryFile().name
        data_store_1 = SQLDataStore(storage,
                                    connection_string=f"sqlite:///{db}",
                                    automigrate=True)
        data_store_2 = SQLDataStore(storage,
                                    connection_string=f"sqlite:///{db}",
                                    automigrate=False)
        yield data_store_1, data_store_2

    finally:
        data_store_1.watcher_keep_running = False
        data_store_2.watcher_keep_running = False


def add_test_job(job_name, database):
    with database.session(reraise=True) as session:
        session.add(models.Job(
            name=job_name,
            action_digest="test-action-digest/144",
            priority=10,
            stage=OperationStage.CACHE_CHECK.value
        ))


def populate_database(database):
    with database.session(reraise=True) as session:
        session.add_all([
            models.Job(
                name="test-job",
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name="test-operation"
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            ),
            models.Job(
                name="other-job",
                action_digest="other-action/10",
                priority=5,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="other-operation"
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="extra-job",
                action_digest="extra-action/50",
                priority=20,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="extra-operation"
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"], "generic": ["requirement"]})
            ),
            models.Job(
                name="cancelled-job",
                action_digest="cancelled-action/35",
                priority=20,
                stage=OperationStage.COMPLETED.value,
                cancelled=True,
                queued_timestamp=datetime(2019, 6, 1),
                queued_time_duration=60,
                worker_start_timestamp=datetime(2019, 6, 1, minute=1),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.CANCELLED.value
                )],
                operations=[
                    models.Operation(
                        name="cancelled-operation",
                        cancelled=True
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="finished-job",
                action_digest="finished-action/35",
                priority=20,
                stage=OperationStage.COMPLETED.value,
                queued_timestamp=datetime(2019, 6, 1),
                queued_time_duration=10,
                worker_start_timestamp=datetime(2019, 6, 1, second=10),
                worker_completed_timestamp=datetime(2019, 6, 1, minute=1),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.COMPLETED.value
                )],
                operations=[
                    models.Operation(
                        name="finished-operation"
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="platform-job",
                action_digest="platform-action/10",
                priority=5,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="platform-operation"
                    )
                ],
                platform_requirements=hash_from_dict(
                    {"OSFamily": ["aix"], "generic": ["requirement", "requirement2"]}),
            )
        ])


@pytest.mark.parametrize("conn_str", ["sqlite:///file:memdb1?option=value&cache=shared&mode=memory",
                                      "sqlite:///file:memdb1?mode=memory&cache=shared",
                                      "sqlite:///file:memdb1?cache=shared&mode=memory",
                                      "sqlite:///file::memory:?cache=shared",
                                      "sqlite:///file::memory:",
                                      "sqlite:///:memory:",
                                      "sqlite:///",
                                      "sqlite://"])
def test_is_sqlite_inmemory_connection_string(conn_str):
    with pytest.raises(ValueError):
        # Should raise ValueError when trying to instantiate
        database = SQLDataStore(None, connection_string=conn_str)


@pytest.mark.parametrize("conn_str", ["sqlite:///../../myfile.db",
                                      "sqlite:///./myfile.db",
                                      "sqlite:////myfile.db"])
def test_file_based_sqlite_db(conn_str):
    # Those should be OK and not raise anything during instantiation
    with mock.patch('buildgrid.server.persistence.sql.impl.create_engine') as create_engine:
        database = SQLDataStore(None, connection_string=conn_str)
        database.watcher_keep_running = False
        assert create_engine.call_count == 1
        call_args, call_kwargs = create_engine.call_args


def test_rollback(database):
    job_name = "test-job"
    add_test_job(job_name, database)
    try:
        with database.session(reraise=True) as session:
            job = session.query(models.Job).filter_by(name=job_name).first()
            assert job is not None
            job.name = "other-job"
            raise Exception("Forced exception")
    except Exception:
        pass

    with database.session(reraise=True) as session:
        # This query will only return a result if the rollback was successful and
        # the job name wasn't changed
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None


def test_get_job_by_action(database):
    populate_database(database)
    job = database.get_job_by_action("notarealjob")
    assert job is None

    # Ensure that get_job_by_action doesn't get completed jobs.
    # Actions aren't unique in the job history, so we only care
    # about the one that is currently incomplete (if any).
    job = database.get_job_by_action(models.string_to_digest("finished-action/35"))
    assert job is None

    job = database.get_job_by_action(models.string_to_digest("extra-action/50"))
    assert job.name == "extra-job"
    assert job.priority == 20


def test_get_job_by_name(database):
    populate_database(database)
    job = database.get_job_by_name("notarealjob")
    assert job is None

    job = database.get_job_by_name("extra-job")
    assert job.name == "extra-job"
    assert job.priority == 20


def test_get_job_by_operation(database):
    populate_database(database)
    job = database.get_job_by_operation("notarealjob")
    assert job is None

    job = database.get_job_by_operation("extra-operation")
    assert job.name == "extra-job"
    assert job.priority == 20


def test_get_all_jobs(database):
    populate_database(database)
    jobs = database.get_all_jobs()
    assert len(jobs) == 4


def test_hash_from_dict(database):
    config = {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    assert hash_from_dict(config) == '2844e4e6d221f4205cdb70c344f51db79dc1bd80'


def test_flatten_capabilities(database):
    capabilities = {}
    assert database.flatten_capabilities(capabilities) == []

    capabilities = {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    expected_flattened_capabilities = sorted([('OSFamily', 'Linux'), ('ISA', 'x86-32'), ('ISA', 'x86-64')])
    assert sorted(database.flatten_capabilities(capabilities)) == expected_flattened_capabilities

    # Changing the order of the ISA values should produce the same flattened capabilities
    capabilities = {'OSFamily': ['Linux'], 'ISA': ['x86-64', 'x86-32']}
    assert sorted(database.flatten_capabilities(capabilities)) == expected_flattened_capabilities


def compare_lists_of_dicts(list1, list2):
    for dictionary in list1:
        assert dictionary in list2
    for dictionary in list2:
        assert dictionary in list1


def test_get_partial_capabilities(database):
    capabilities = {}
    assert list(database.get_partial_capabilities(capabilities)) == [{}]

    def sortfunc(d):
        return sorted((k, v) for k, v in d.items())

    expected_partial_capabilities = sorted([
        {},
        {'OSFamily': ['Linux']},
        {'ISA': ['x86-32']},
        {'ISA': ['x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-64']},
        {'ISA': ['x86-32', 'x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    ], key=sortfunc)

    capabilities = {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    compare_lists_of_dicts(list(database.get_partial_capabilities(capabilities)), expected_partial_capabilities)


def test_get_partial_capabilities_hashes(database):
    capabilities = {}
    assert database.get_partial_capabilities_hashes(capabilities) == [
        hashlib.sha1(json.dumps(capabilities, sort_keys=True).encode()).hexdigest()]

    expected_partial_capabilities = [
        {},
        {'OSFamily': ['Linux']},
        {'ISA': ['x86-32']},
        {'ISA': ['x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-64']},
        {'ISA': ['x86-32', 'x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    ]
    expected_partial_capabilities_hashes = sorted(list(map(
        lambda cap: hashlib.sha1(json.dumps(cap, sort_keys=True).encode()).hexdigest(),
        expected_partial_capabilities)))

    capabilities = {'OSFamily': 'Linux', 'ISA': ['x86-32', 'x86-64']}
    assert sorted(database.get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes

    # Should be the same if the string is passed in as a singleton list
    capabilities = {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    assert sorted(database.get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes

    # Changing the order of the ISA values should produce the same hashes
    capabilities = {'OSFamily': 'Linux', 'ISA': ['x86-64', 'x86-32']}
    assert sorted(database.get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes


def test_create_job(database):
    job_name = "test-job"
    job = Job(do_not_cache=False,
              action_digest=models.string_to_digest("test-action-digest/144"),
              priority=10,
              name=job_name)
    database.create_job(job)

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None
        assert job.priority == 10
        assert job.action_digest == "test-action-digest/144"
        assert not job.do_not_cache
        assert job.queued_timestamp is None
        assert job.worker_start_timestamp is None
        assert job.worker_completed_timestamp is None


def test_update_job(database):
    job_name = "test-job"
    add_test_job(job_name, database)

    database.update_job(job_name, {"priority": 1})

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None
        assert job.priority == 1


def test_delete_job(database):
    populate_database(database)
    job = database.get_job_by_name("test-job")
    database.store_response(job)
    assert "test-job" in database.response_cache

    database.delete_job("test-job")
    assert "test-job" not in database.response_cache


def test_store_response(database):
    populate_database(database)
    job = database.get_job_by_name("test-job")
    database.store_response(job)

    updated = database.get_job_by_name("test-job")
    assert updated.execute_response is not None
    assert "test-job" in database.response_cache
    assert database.response_cache["test-job"] is not None


def test_get_operations_by_stage(database):
    populate_database(database)
    operations = database.get_operations_by_stage(OperationStage(4))
    assert len(operations) == 2


def test_list_operations(database):
    populate_database(database)
    operations, next_page_token = database.list_operations()
    # Only incomplete operations are returned
    assert len(operations) == 6
    assert next_page_token == ""


def test_list_operations_filtered(database):
    populate_database(database)
    operation_filters = FilterParser.parse_listoperations_filters("stage != completed")
    operations, next_page_token = database.list_operations(
        operation_filters=operation_filters
    )
    assert len(operations) == 4
    assert next_page_token == ""


def test_list_operations_sort_only(database):
    populate_database(database)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name(asc)")
    operations, next_page_token = database.list_operations(operation_filters)
    # Only incomplete operations are returned
    assert len(operations) == 6
    assert next_page_token == ""


def check_operation_order(operations, sort_keys, database):
    with database.session() as session:
        results = session.query(models.Operation).join(models.Job, models.Operation.job_name == models.Job.name)
        results = results.order_by(*sort_keys)
        for expected_operation, actual_operation in zip(results, operations):
            if expected_operation.name != actual_operation.name:
                return False

    return True


def test_list_operations_sorted(database):
    def check_sort_filters(filter_string, sort_columns):
        operation_filters = FilterParser.parse_listoperations_filters(filter_string)
        operations, _ = database.list_operations(
            operation_filters=operation_filters
        )
        assert check_operation_order(operations, sort_columns, database)

    populate_database(database)
    # By default, operations are primarily sorted by queued_timestamp,
    check_sort_filters("", [models.Job.queued_timestamp, models.Operation.name])

    # When "name" is given as the "sort_order", the operations should
    # be ordered primarily by name in ascending order
    check_sort_filters("sort_order = name", [models.Operation.name])

    # This is equivalent to putting "(asc)" at the end of the sort order value
    check_sort_filters("sort_order = name(asc)", [models.Operation.name])

    # "(desc)" at the end of the sort_order value should produce descending order
    check_sort_filters("sort_order = name(desc)", [models.Operation.name.desc()])

    # The parser requires that (desc) be attached to the sort key name.
    # Whitespace isn't supported.
    with pytest.raises(InvalidArgumentError):
        check_sort_filters("sort_order = name (desc)", [models.Operation.name.desc()])


def test_large_list_operations_multi_job(database):
    """ With a large number of jobs and corresponding operations in the database,
    ensure that the results are properly returned in page form.

    We populate the database with 2500 operations and ask for 1000-operation pages.
    We should get back two 1000-operation pages and one 500-operation page. """
    queued_timestamp = datetime.utcnow()
    total_jobs = 2500
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-job-{i}",
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:04}"
                    )
                ],
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(total_jobs)
        ]
        session.add_all(jobs)

    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    token_timestamp = (queued_timestamp + timedelta(seconds=999)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0999"

    # Another 1000 are returned the second time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 1000
    token_timestamp = (queued_timestamp + timedelta(seconds=1999)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-1999"

    # The last 500 are returned the last time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 500
    assert next_token == ""


def test_large_list_operations_same_job(database):
    """ With a large number of operations in the database, ensure that the results
    are properly returned in page form. All of the operations are for a single job.

    We populate the database with 2500 operations and ask for 1000-operation pages.
    We should get back two 1000-operation pages and one 500-operation page. """
    queued_timestamp = datetime.utcnow()
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:04}"
                    ) for i in range(2500)
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    token_timestamp = queued_timestamp.strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0999"

    # Another 1000 are returned the second time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 1000
    assert next_token == f"{token_timestamp}|test-operation-1999"

    # The last 500 are returned the last time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 500
    assert next_token == ""


def test_large_list_operations_filtered(database):
    """  We populate the database with 1000 operations and filter for the operations
    [250, 750). We should get back those 500 operations. """
    queued_timestamp = datetime.utcnow()
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:03}"
                    ) for i in range(1000)
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    op_filters = FilterParser().parse_listoperations_filters(
        "name >= test-operation-250 & name < test-operation-750")
    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(operation_filters=op_filters)
    assert len(operations) == 500
    for operation in operations:
        assert operation.name >= "test-operation-250" and operation.name < "test-operation-750"
    assert next_token == ""


def test_paginated_custom_sort(database):
    """ Demonstrate that custom sort orders work on a paginated result set. """
    queued_timestamp = datetime.utcnow()
    total_jobs = 100
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-job-{i}",
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{total_jobs-i-1:04}"
                    )
                ],
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(total_jobs)
        ]
        session.add_all(jobs)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name")
    page_size = 10
    next_page_token = ""
    for _ in range(total_jobs // page_size):
        operations, next_page_token = database.list_operations(
            operation_filters=operation_filters,
            page_size=page_size,
            page_token=next_page_token
        )
        assert len(operations) == page_size
        prev_operation = None
        for operation in operations:
            if prev_operation:
                assert prev_operation.name < operation.name
            prev_operation = operation


def test_large_list_operations_exact(database):
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i}"
                    ) for i in range(1000)
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    # All operations are returned
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    # There is no next_token since we've gotten all of the results
    assert next_token == ""


def test_lazy_expiry_with_list_operations(database):
    max_execution_timeout_seconds = timedelta(seconds=7200)
    queued_timestamp = datetime.utcnow() - 3 * max_execution_timeout_seconds
    old_worker_start_timestamp = datetime.utcnow() - 2 * max_execution_timeout_seconds
    new_worker_start_timestamp = datetime.utcnow()

    num_jobs_queued = 7
    # Add a bunch of jobs in queued state
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-queued-job-{i}",
                action_digest=f"test-queued-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-queued-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_queued)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a long time
    num_jobs_old_executing = 11
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-executing-old-job-{i}",
                action_digest=f"test-executing-old-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-old-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=old_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_old_executing)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a short time
    num_jobs_new_executing = 13
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-executing-new-job-{i}",
                action_digest=f"test-executing-new-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-new-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=new_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_new_executing)
        ]
        session.add_all(jobs)

    # List all the operations
    # and make sure all the operations that exceed the
    # execution timeout are marked as cancelled

    # Mock the `notify_job_updated` method to inspect the call
    with mock.patch.object(database, '_notify_job_updated',
                           autospec=True) as mock_notify_fn:
        # Call list operations with the default filters (i.e. ignore COMPLETED operations)
        operations, _ = database.list_operations(
            page_size=1000,
            operation_filters=DEFAULT_OPERATION_FILTERS,
            max_execution_timeout=max_execution_timeout_seconds.seconds)

        # Make sure we did a notify for each of the cancelled jobs
        call_args, _ = mock_notify_fn.call_args
        assert mock_notify_fn.call_count == 1
        jobs_cancelled_notifications = call_args[0]

        count_cancelled_old = sum(('test-executing-old-job-' in job)
                                  for job in jobs_cancelled_notifications)

        # Make sure all the jobs that were old were cancelled
        # and that all the cancelled jobs were the old ones
        assert len(jobs_cancelled_notifications) == num_jobs_old_executing
        assert count_cancelled_old == num_jobs_old_executing

    # The queued and newly executing jobs should be in the list
    assert sum('test-operation-queued-' in op.name
               for op in operations) == num_jobs_queued
    assert sum('test-operation-new-' in op.name
               for op in operations) == num_jobs_new_executing

    # The jobs that have been executing for a while should be cancelled
    assert sum('test-operation-old-' in op.name for op in operations) == 0


def test_create_operation(database):
    job_name = "test-job"
    add_test_job(job_name, database)

    op_name = "test-operation"

    database.create_operation(op_name, job_name)

    with database.session(reraise=True) as session:
        op = session.query(models.Operation).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name


def test_update_operation(database):
    job_name = "test-job"
    add_test_job(job_name, database)

    op_name = "test-operation"
    with database.session(reraise=True) as session:
        session.add(models.Operation(
            name=op_name,
            job_name=job_name,
            cancelled=False
        ))

    database.update_operation(op_name, {"cancelled": True})

    with database.session(reraise=True) as session:
        op = session.query(models.Operation).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name
        assert op.cancelled


def test_get_leases_by_state(database):
    populate_database(database)
    leases = database.get_leases_by_state(LeaseState(1))
    assert len(leases) == 3


def test_create_lease(database):
    job_name = "test-job"
    add_test_job(job_name, database)

    state = 0
    lease = bots_pb2.Lease()
    lease.id = job_name
    lease.state = state

    database.create_lease(lease)

    with database.session(reraise=True) as session:
        lease = session.query(models.Lease).filter_by(job_name=job_name).first()
        assert lease is not None
        assert lease.job.name == job_name
        assert lease.state == state


def test_update_lease(database):
    job_name = "test-job"
    add_test_job(job_name, database)

    state = 0
    with database.session(reraise=True) as session:
        session.add(models.Lease(
            job_name=job_name,
            state=state
        ))

    database.update_lease(job_name, {"state": 1})
    with database.session(reraise=True) as session:
        lease = session.query(models.Lease).filter_by(job_name=job_name).first()
        assert lease is not None
        assert lease.job.name == job_name
        assert lease.state == 1


def test_load_unfinished_jobs(database):
    populate_database(database)

    jobs = database.load_unfinished_jobs()
    assert jobs
    assert jobs[0].name == "test-job"


def test_assign_lease_for_next_job(database):
    populate_database(database)

    def cb(j):
        lease = j.lease
        if not lease:
            lease = j.create_lease("test-suite")
        if lease:
            j.mark_worker_started()
            return [lease]
        return []

    # The highest priority runnable job with requirements matching these
    # capabilities is other-job, which is priority 5 and only requires linux
    leases = database.assign_lease_for_next_job({"OSFamily": ["linux"]}, cb)
    assert len(leases) == 1
    assert leases[0].id == "other-job"

    database.queue_job("other-job")

    # The highest priority runnable job for these capabilities is still
    # other-job, since priority 5 is more urgent than the priority 20 of
    # example-job. test-job has priority 1, but its requirements are not
    # fulfilled by these capabilities
    leases = database.assign_lease_for_next_job(
        {"OSFamily": ["linux"], "generic": ["requirement"]}, cb)
    assert len(leases) == 1
    assert leases[0].id == "other-job"

    # The highest priority runnable job for this magical machine which has
    # multiple values for the `os` capability is test-job, since its requirements
    # are fulfilled and it has priority 1, compared with priority 5 for
    # other-job
    leases = database.assign_lease_for_next_job(
        {"OSFamily": ["linux", "sunos"]}, cb)
    assert len(leases) == 1
    assert leases[0].id == "test-job"

    # Shouldn't match with platform-job, worker only has one of the two
    # requirements
    leases = database.assign_lease_for_next_job(
        {"OSFamily": ["aix"], "generic": ["requirement"]}, cb)
    assert len(leases) == 0

    # Should match with platform-job, has exact specification needed
    leases = database.assign_lease_for_next_job(
        {"OSFamily": ["aix"], "generic": ["requirement", "requirement2"]}, cb)
    assert len(leases) == 1
    assert leases[0].id == "platform-job"

    database.queue_job("platform-job")

    # Should match with platform-job, worker has superset of specifications
    specifications = {
        "OSFamily": ["aix", "android"],
        "generic": ["requirement", "requirement2", "requirement3"]
    }
    leases = database.assign_lease_for_next_job(specifications, cb)
    assert len(leases) == 1
    assert leases[0].id == "platform-job"


def test_assign_lease_to_oldest_job(database):
    # Test that jobs within the same priority get scheduled in a FIFO
    # manner, based on the job's queued_timestamp
    def cb(j):
        lease = j.lease
        if not lease:
            lease = j.create_lease("test-suite")
        if lease:
            j.mark_worker_started()
            return [lease]
        return []

    test_queuetime = datetime.utcnow()
    with database.session(reraise=True) as session:
        session.add_all([
            models.Job(
                name="first-job",
                action_digest="test-action/100",
                priority=5,
                stage=OperationStage.QUEUED.value,
                queued_timestamp=test_queuetime - timedelta(seconds=5),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="test-operation"
                    )
                ],
                platform_requirements=hash_from_dict({})
            ),
            models.Job(
                name="second-job",
                action_digest="other-action/10",
                priority=5,
                stage=OperationStage.QUEUED.value,
                queued_timestamp=test_queuetime - timedelta(seconds=10),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="other-operation"
                    )
                ],
                platform_requirements=hash_from_dict({})
            ),
            models.Job(
                name="high-priority-job",
                action_digest="high-priority-action/10",
                priority=1,
                stage=OperationStage.QUEUED.value,
                queued_timestamp=test_queuetime,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="high-priority-operation"
                    )
                ],
                platform_requirements=hash_from_dict({})
            ),
        ])

    # This job has a higher priority, so it goes first
    # even though it has the newest timestamp
    leases = database.assign_lease_for_next_job({}, cb)
    assert len(leases) == 1
    assert leases[0].id == "high-priority-job"

    # Rest of jobs have the same priority, take the one with the
    # oldest queuetime first
    leases = database.assign_lease_for_next_job({}, cb)
    assert len(leases) == 1
    assert leases[0].id == "second-job"

    leases = database.assign_lease_for_next_job({}, cb)
    assert len(leases) == 1
    assert leases[0].id == "first-job"


def test_to_internal_job(database):
    populate_database(database)

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name="finished-job").first()
        internal_job = job.to_internal_job(database)

        assert isinstance(internal_job.queued_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_start_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_completed_timestamp_as_datetime, datetime)

        assert internal_job.operation_stage.value == 4

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name="cancelled-job").first()
        internal_job = job.to_internal_job(database)

        assert isinstance(internal_job.queued_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_start_timestamp_as_datetime, datetime)
        assert internal_job.worker_completed_timestamp_as_datetime is None

        assert internal_job.cancelled
        assert internal_job.operation_stage.value == 4


# Check that the metrics returned match what is
# populated in the test database
def test_get_metrics(database):
    populate_database(database)

    # Setup expected counts for each category
    expected_metrics = {}
    expected_metrics[MetricCategories.LEASES.value] = {
        LeaseState.UNSPECIFIED.value: 0,
        LeaseState.PENDING.value: 3,
        LeaseState.COMPLETED.value: 1,
        LeaseState.CANCELLED.value: 1,
        LeaseState.ACTIVE.value: 1
    }
    expected_metrics[MetricCategories.OPERATIONS.value] = {
        OperationStage.UNKNOWN.value: 0,
        OperationStage.CACHE_CHECK.value: 0,
        OperationStage.QUEUED.value: 4,
        OperationStage.EXECUTING.value: 0,
        OperationStage.COMPLETED.value: 2
    }
    expected_metrics[MetricCategories.JOBS.value] = {
        OperationStage.UNKNOWN.value: 0,
        OperationStage.CACHE_CHECK.value: 0,
        OperationStage.QUEUED.value: 4,
        OperationStage.EXECUTING.value: 0,
        OperationStage.COMPLETED.value: 2
    }

    returned_metrics = database.get_metrics()
    assert returned_metrics != {}
    for category in MetricCategories:
        assert category.value in returned_metrics
        assert expected_metrics[category.value] == returned_metrics[category.value]


# Make it so connection to the database throws a
# DatabaseError, and ensure it's non-fatal
def test_get_metrics_error(database):
    populate_database(database)

    with mock.patch.object(database, 'session', autospec=True,
                           side_effect=DatabaseError("TestException")) as session_fn:
        returned_metrics = database.get_metrics()
        assert session_fn.called
        assert returned_metrics == {}


def test_watcher_exits_after_flag_update(database):
    poll_interval = 0.02
    delta = 0.5

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Thread should be running in a loop already
    for i in range(3):
        assert database.watcher.is_alive()
        sleep(poll_interval + delta)

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False


def test_watcher_polling_handles_exceptions(database):
    def _failing_get_watched_jobs():
        raise Exception("db-fail")

    poll_interval = 0.02
    delta = 0.5

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Thread should be running already
    assert database.watcher.is_alive()

    # Simulate database failure by patching this method to raise
    database._get_watched_jobs = _failing_get_watched_jobs

    # Make sure the watcher thread is still running even when
    # the database keeps raising exceptions
    for i in range(3):
        assert database.watcher.is_alive()
        sleep(poll_interval + delta)

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False


def test_external_job_completion_loads_result(database_pair):
    database, database_external = database_pair

    # Create job in-memory
    job_name = "test-job-external-completion"
    platform_reqs = {"OSFamily": set(["solaris"])}
    inmem_job = Job(True,
                    models.string_to_digest("test-action/100"),
                    name=job_name,
                    priority=1,
                    stage=OperationStage.CACHE_CHECK.value,
                    operation_names=["test-operation"],
                    platform_requirements=platform_reqs)
    # Add job to db
    database.create_job(inmem_job)

    # Create a dummy ActionResult
    output_file_1 = remote_execution_pb2.OutputFile(
        path="/path/to/file1.txt",
        digest=models.string_to_digest("output_file_1_digest/123"))
    output_directory_1 = remote_execution_pb2.OutputDirectory(
        path="/path/to/dir1",
        tree_digest=models.string_to_digest("output_dir_1_digest/456"))

    action_result = remote_execution_pb2.ActionResult()
    action_result.output_files.extend([output_file_1])
    action_result.output_directories.extend([output_directory_1])
    action_result.exit_code = 0
    action_result.stdout_raw = str.encode("some-stdout")

    action_result_in_any_proto = any_pb2.Any()
    action_result_in_any_proto.Pack(action_result)

    # Update job to completed with a result "externally"
    inmem_job_db2 = database_external.get_job_by_name(job_name)

    # Simulate lease assignment/job completion
    state = 0
    lease = bots_pb2.Lease()
    lease.id = job_name
    lease.state = state

    database_external.create_lease(lease)

    # Reload job with lease
    inmem_job_db2 = database_external.get_job_by_name(job_name)
    inmem_job_db2.update_lease_state(LeaseState.ACTIVE, data_store=database_external)
    # Reload job (has a lease now)
    inmem_job_db2.update_lease_state(LeaseState.COMPLETED,
                                     status=status_pb2.Status(code=0),
                                     result=action_result_in_any_proto,
                                     action_cache=None,
                                     data_store=database_external,
                                     skip_notify=True)
    inmem_job_db2.update_operation_stage(OperationStage.COMPLETED,
                                         data_store=database_external)

    # Reload job from db1 and check for result being present
    inmem_job = database.get_job_by_name(job_name)
    inmem_job_serialized = inmem_job.action_result.SerializeToString()
    action_result_serialized = action_result.SerializeToString()

    # Compare fields that were set from the "worker"
    # as ActionResult may contain additional metadata, like timings etc
    assert inmem_job.action_result.output_files == action_result.output_files
    assert inmem_job.action_result.output_directories == action_result.output_directories
    assert inmem_job.action_result.exit_code == action_result.exit_code
    assert inmem_job.action_result.stdout_raw == action_result.stdout_raw
