# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import re
from unittest import mock
import uuid

import grpc
from grpc._server import _Context
import pytest

from buildgrid._protos.google.bytestream.bytestream_pb2 import (
    QueryWriteStatusRequest,
    ReadRequest,
    ReadResponse,
    WriteRequest,
    WriteResponse
)
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import (
    CreateLogStreamRequest,
    LogStream
)
from buildgrid.server.cas.service import ByteStreamService
from buildgrid.server.cas.instance import ByteStreamInstance
from buildgrid.server.logstream import service
from buildgrid.server.logstream.instance import LogStreamInstance
from buildgrid.server.logstream.service import LogStreamService
from buildgrid.server.logstream.streaming.memory import StreamStorage


server = mock.MagicMock()


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


@pytest.fixture()
def storage(request):
    yield StreamStorage()


@pytest.fixture(params=['', 'test-'])
def logstream_instance(request, storage):
    yield LogStreamInstance(request.param, storage=storage)


@pytest.fixture()
def bytestream_instance(request, storage):
    yield ByteStreamInstance(stream_storage=storage)


@pytest.fixture()
def logstream_service(logstream_instance):
    with mock.patch.object(service, 'remote_logstream_pb2_grpc'):
        logstream = LogStreamService(server)
        logstream.add_instance('', logstream_instance)
        yield logstream


@pytest.fixture()
def bytestream_service(bytestream_instance):
    with mock.patch.object(service, 'bytestream_pb2_grpc'):
        bytestream = ByteStreamService(server)
        bytestream.add_instance('', bytestream_instance)
        yield bytestream


def test_create_logstream(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response = logstream_service.CreateLogStream(request, context)
    assert isinstance(response, LogStream)

    instance = logstream_service._get_instance('')
    match = re.fullmatch(
        f'^{parent}/logStreams/{instance._prefix}[0-9a-f-]*$', response.name)
    assert match is not None


def test_create_logstream_repeated_calls(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response_1 = logstream_service.CreateLogStream(request, context)
    response_2 = logstream_service.CreateLogStream(request, context)
    assert response_1 == response_2


def test_logstream_bytestream_read_no_stream(bytestream_service, context):
    resource = '/test/logStreams/abcdef'
    request = ReadRequest(resource_name=resource)
    responses = bytestream_service.Read(request, context)
    response = next(responses)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)
    assert response == ReadResponse()


def test_logstream_bytestream_write_no_stream(bytestream_service, context):
    resource = '/test/logStreams/abcdef/abcdef'

    def requests():
        yield WriteRequest(resource_name=resource, data=b'test')

    response = bytestream_service.Write(requests(), context)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)
    assert response == WriteResponse()


def test_logstream_bytestream_read(logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'{parent}')
    response = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(response.name)
    stream._content.append(b'test log entry')

    stream_resource_name = f'/{response.name}'
    request = ReadRequest(resource_name=stream_resource_name)
    responses = bytestream_service.Read(request, context)
    response = next(responses)
    assert response.data == b'test log entry'


def test_logstream_bytestream_write(logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(response.name)
    assert not stream._content

    stream_resource_name = f'/{response.write_resource_name}'

    def requests():
        yield WriteRequest(resource_name=stream_resource_name, data=b'test log entry')

    response = bytestream_service.Write(requests(), context)
    assert len(stream._content) == 1
    assert stream._content[0] == b'test log entry'


def test_logstream_bytestream_write_specific_resource_name(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(logstream.name)
    assert not stream._content

    def requests(resource):
        yield WriteRequest(resource_name=resource, data=b'test log entry')

    stream_resource_name = f'/{logstream.name}'
    response = bytestream_service.Write(requests(stream_resource_name), context)
    context.set_code.assert_called_with(grpc.StatusCode.INVALID_ARGUMENT)
    assert response == WriteResponse()
    assert not stream._content

    stream_resource_name = f'/{logstream.write_resource_name}'
    response = bytestream_service.Write(requests(stream_resource_name), context)
    assert len(stream._content) == 1
    assert stream._content[0] == b'test log entry'


def test_logstream_bytestream_cleanup_write_only(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(logstream.name)

    def requests(resource):
        yield WriteRequest(resource_name=resource,
                           data=b'test log entry',
                           finish_write=True)

    # Check that committing a Write is successfully tracked, and the
    # stream is cleaned up
    write_name = f'/{logstream.write_resource_name}'
    response = bytestream_service.Write(requests(write_name), context)
    assert stream.completed
    assert parent not in instance._stream_store._streams


def test_logstream_bytestream_cleanup_after_read(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(logstream.name)
    # Temporarily add a fake reader so that the write doesn't cause
    # cleanup before we want it. Forcing this here allows us to write
    # and then read sequentially rather than making a complex test
    # case running a LogStream in a subprocess.
    stream._reader_count = 1

    def requests(resource):
        yield WriteRequest(resource_name=resource,
                           data=b'test log entry',
                           finish_write=True)

    # Check that committing a Write is successfully tracked, and the
    # stream is cleaned up
    write_name = f'/{logstream.write_resource_name}'
    write_response = bytestream_service.Write(requests(write_name), context)

    # Force the stream to not be committed, since we're pretending this
    # is running in parallel.
    stream._committed = False
    read_name = f'/{logstream.name}'
    read_request = ReadRequest(resource_name=read_name)
    responses = bytestream_service.Read(read_request, context)
    read_response = next(responses)
    assert read_response.data == b'test log entry'

    # Re-commit the stream and remove the fake reader
    stream._committed = True
    stream._reader_count -= 1
    assert stream._reader_count > 0

    # Test the disconnection callback
    bs_instance = bytestream_service._get_instance('')
    bs_instance.disconnect_logstream_reader(stream.name)

    # Now the stream should be marked as completed and cleaned up
    assert stream.completed
    assert parent not in instance._stream_store._streams


def test_logstream_bytestream_query_write_status_blocks_with_no_reader(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(logstream.name)
    assert not stream.has_readers()

    query_write_status = QueryWriteStatusRequest(
        resource_name=logstream.write_resource_name)

    active_statuses = [False, True]

    def is_active():
        return active_statuses.pop()

    context.is_active = is_active

    bs_instance = bytestream_service._get_instance('')
    bs_instance._query_activity_timeout = 2
    response = bytestream_service.QueryWriteStatus(query_write_status, context)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)


def test_logstream_bytestream_query_write_status(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')
    stream = instance._stream_store.get_stream(logstream.name)
    assert not stream.has_readers()

    query_write_status = QueryWriteStatusRequest(
        resource_name=logstream.write_resource_name)

    # Pretend that there's a reader.
    stream._reader_count = 1
    response = bytestream_service.QueryWriteStatus(query_write_status, context)
    assert response.committed_size == len(stream)
