# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from contextlib import contextmanager
import functools
import logging
import threading
import time
from typing import Dict, Type, List, Optional
from datetime import timedelta

from buildgrid._enums import MetricRecordDomain, MetricRecordType
from buildgrid._protos.buildgrid.v2.monitoring_pb2 import MetricRecord
from buildgrid.server.monitoring import get_monitoring_bus
from buildgrid._exceptions import BgdError


def create_counter_record(domain: MetricRecordDomain, name: str, count: float, metadata: Dict = None) -> MetricRecord:
    counter_record = MetricRecord()

    counter_record.creation_timestamp.GetCurrentTime()
    counter_record.domain = domain.value
    counter_record.type = MetricRecordType.COUNTER.value
    counter_record.name = name
    counter_record.count = count
    if metadata is not None:
        counter_record.metadata.update(metadata)

    return counter_record


def create_gauge_record(domain: MetricRecordDomain, name: str, value: int, metadata: Dict = None) -> MetricRecord:
    gauge_record = MetricRecord()

    gauge_record.creation_timestamp.GetCurrentTime()
    gauge_record.domain = domain.value
    gauge_record.type = MetricRecordType.GAUGE.value
    gauge_record.name = name
    gauge_record.value = value
    if metadata is not None:
        gauge_record.metadata.update(metadata)

    return gauge_record


def create_timer_record(domain: MetricRecordDomain, name: str, duration: timedelta, metadata=None) -> MetricRecord:
    timer_record = MetricRecord()

    timer_record.creation_timestamp.GetCurrentTime()
    timer_record.domain = domain.value
    timer_record.type = MetricRecordType.TIMER.value
    timer_record.name = name
    timer_record.duration.FromTimedelta(duration)
    if metadata is not None:
        timer_record.metadata.update(metadata)

    return timer_record


def create_distribution_record(domain: MetricRecordDomain, name: str, value: float, metadata=None) -> MetricRecord:
    dist_record = MetricRecord()

    dist_record.creation_timestamp.GetCurrentTime()
    dist_record.domain = domain.value
    dist_record.type = MetricRecordType.DISTRIBUTION.value
    dist_record.name = name
    dist_record.count = value
    if metadata is not None:
        dist_record.metadata.update(metadata)

    return dist_record


def publish_counter_metric(domain: MetricRecordDomain,
                           name: str, count: float, metadata: Dict = None) -> None:
    record = create_counter_record(domain, name, count, metadata)
    monitoring_bus = get_monitoring_bus()
    monitoring_bus.send_record_nowait(record)


def publish_timer_metric(domain: MetricRecordDomain,
                         name: str, duration: timedelta, metadata: Dict = None) -> None:
    record = create_timer_record(domain, name, duration, metadata)
    monitoring_bus = get_monitoring_bus()
    monitoring_bus.send_record_nowait(record)


class DurationMetric:

    """ Provides a decorator and a context manager to measure execution duration. """

    def __init__(self, metric_name: str,
                 metric_domain: MetricRecordDomain = MetricRecordDomain.UNKNOWN,
                 instance_name: str = '',
                 instanced: bool = False):
        self._logger = logging.getLogger(__name__)

        self._metric_name = metric_name
        self._metric_domain = metric_domain
        self._instance_name = instance_name
        self._instanced = instanced

        self._start_time = None

    def __call__(self, func):
        @functools.wraps(func)
        def _timer_wrapper(obj, *args, **kwargs):
            if self._instanced:
                if obj._instance_name is not None:
                    self._instance_name = obj._instance_name
            try:
                start_time = time.perf_counter()
            except AttributeError:
                raise
            except:
                self._logger.exception(f"Error raised while starting timing metric [{self._metric_name}]")

            value = func(obj, *args, **kwargs)

            try:
                self._stop_timer_and_submit(start_time)
            except (AssertionError, AttributeError):
                raise
            except:
                self._logger.exception(f"Error raised while timing metric [{self._metric_name}]")
            return value
        return _timer_wrapper

    def __enter__(self):
        try:
            self._start_time = time.perf_counter()
        except:
            self._logger.exception(f"Error raised while entering timing metric [{self._metric_name}]")
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        try:
            self._stop_timer_and_submit(self._start_time)
        except:
            self._logger.exception(f"Error raised while stopping timing metric [{self._metric_name}] in exit")
        finally:
            self._start_time = None

    def _stop_timer_and_submit(self, start_time):
        monitoring_bus = get_monitoring_bus()
        if self._instanced:
            assert self._instance_name is not None

        run_time = timedelta(seconds=time.perf_counter() - start_time)

        metadata = None
        if self._instanced:
            metadata = {'instance-name': self._instance_name}
        record = create_timer_record(
            self._metric_domain, self._metric_name, run_time, metadata)
        monitoring_bus.send_record_nowait(record)


class Counter():
    """ Provides a generic metric counter. Optionally/Ideally used as a context manager.
        Example Usage:

        with Counter("count-size") as size_counter:
            for i in range(10):
                size_counter.increment(i)
    """

    def __init__(self, metric_name: str, instance_name: Optional[str] = None,
                 metric_domain: MetricRecordDomain = MetricRecordDomain.UNKNOWN):
        self._metric_name = metric_name
        self._instance_name = instance_name
        self._metric_domain = metric_domain
        self._count = 0.0
        self._counter_lock = threading.Lock()

    @property
    def count(self) -> float:
        return self._count

    @count.setter
    def count(self, value: float) -> None:
        with self._counter_lock:
            self._count = value

    @property
    def metric_name(self) -> str:
        return self._metric_name

    @property
    def instance_name(self) -> Optional[str]:
        return self._instance_name

    @instance_name.setter
    def instance_name(self, name: str) -> None:
        with self._counter_lock:
            self._instance_name = name

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        if exception_type is None:
            with self._counter_lock:
                self.publish()

    def increment(self, value: float = 1.0) -> None:
        with self._counter_lock:
            self._count += value

    def publish(self, reset_counter=True) -> None:
        monitoring_bus = get_monitoring_bus()

        metadata = None
        if self._instance_name is not None:
            metadata = {'instance-name': self._instance_name}

        record = create_counter_record(self._metric_domain, self._metric_name,
                                       self._count, metadata)
        monitoring_bus.send_record_nowait(record)
        if reset_counter:
            self._count = 0.0


class ExceptionCounter(Counter):
    """ Provides a decorator and context manager in order to count exceptions thrown in a function/method body.
        This class inherits from Counter, publishing a value of 1, using the base classes methods.
        Example Usage:

        with ExceptionCounter("test", exceptions=[RuntimeError]) as ec:
            ret_val = do_work()
    """

    def __init__(self, metric_name: str, *args,
                 exceptions: List[Type[Exception]] = [BgdError], **kwargs):

        super().__init__(metric_name, *args, **kwargs)

        # Create tuple so catching the exceptions is seamless
        self._exceptions = tuple(exceptions)

        # Increment the counter to 1, publishing will occur on every exception caught.
        self.increment()

    def __exit__(self, exception_type, exception_value, traceback):
        if exception_value is not None:
            for exception in self._exceptions:
                if isinstance(exception_value, exception):
                    self.publish()
                    return

    def __call__(self, func):
        @functools.wraps(func)
        def _exception_wrapper(obj, *args, **kwargs):
            try:
                return func(obj, *args, **kwargs)
            except self._exceptions as e:
                with self._counter_lock:
                    if hasattr(obj, '_instance_name'):
                        self._instance_name = obj._instance_name
                    try:
                        self.publish(reset_counter=False)
                    except Exception:
                        logging.getLogger(__name__).exception(f"Expection raised when publishing \
                                                exception metric of type: {type(e)}.")
                raise e
        return _exception_wrapper


class Distribution(Counter):
    """ Provides a generic metric using Distribution semantics """

    def __init__(self, metric_name: str, instance_name: str = "",
                 metric_domain: MetricRecordDomain = MetricRecordDomain.UNKNOWN):
        super().__init__(metric_name, instance_name, metric_domain)

    def publish(self, reset=True) -> None:
        monitoring_bus = get_monitoring_bus()

        metadata = {'instance-name': self._instance_name} if self._instance_name else None
        record = create_distribution_record(self._metric_domain, self._metric_name,
                                            self._count, metadata)
        monitoring_bus.send_record_nowait(record)
        if reset:
            self._count = 0.0
