# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from typing import Optional

import redis

from buildgrid._exceptions import NotFoundError
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    ActionResult,
    Digest
)
from buildgrid.server.actioncache.caches.action_cache_abc import ActionCacheABC
from buildgrid.server.cas.storage.redis import redis_client_exception_wrapper
from buildgrid.server.cas.storage.storage_abc import StorageABC


class RedisActionCache(ActionCacheABC):

    def __init__(self, storage: StorageABC, allow_updates: bool=True,
                 cache_failed_actions: bool=True, host: str='localhost',
                 port: int=6379, password: Optional[str]=None, db: int=0):
        ActionCacheABC.__init__(self, storage=storage, allow_updates=allow_updates)

        self._logger = logging.getLogger(__name__)
        self._client = redis.Redis(host=host, port=port, password=password, db=db)
        self._cache_failed_actions = cache_failed_actions

    @redis_client_exception_wrapper
    def get_action_result(self, action_digest: Digest) -> ActionResult:
        key = self._get_key(action_digest)
        serialized_digest = self._client.get(key)
        if serialized_digest:
            storage_digest = Digest.FromString(serialized_digest)
            if storage_digest:
                action_result = self._storage.get_message(storage_digest, ActionResult)

                if action_result is not None:
                    if self._action_result_blobs_still_exist(action_result):
                        return action_result

            if self._allow_updates:
                self._logger.debug(f"Removing {action_digest.hash}/{action_digest.size_bytes}"
                                   "from cache due to missing blobs in CAS")
                self._client.delete(key)

        raise NotFoundError(f"Key not found: [{key}]")

    @redis_client_exception_wrapper
    def update_action_result(self, action_digest: Digest,
                             action_result: ActionResult) -> None:
        if not self._allow_updates:
            raise NotImplementedError("Updating cache not allowed")

        if self._cache_failed_actions or action_result.exit_code == 0:
            result_digest = self._storage.put_message(action_result)
            key = self._get_key(action_digest)
            self._client.set(key, result_digest.SerializeToString())

            self._logger.info(
                f"Result cached for action [{action_digest.hash}/{action_digest.size_bytes}]")

    def _get_key(self, action_digest: Digest) -> str:
        return f'action-cache.{action_digest.hash}_{action_digest.size_bytes}'
