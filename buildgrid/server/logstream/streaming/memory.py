# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from queue import Empty, Queue
from threading import Event, Lock
from typing import Dict, Iterator, List, Optional, Tuple

from buildgrid._exceptions import NotFoundError, StreamAlreadyExistsError
from buildgrid._protos.google.bytestream.bytestream_pb2 import (
    ReadResponse,
    WriteResponse
)
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import LogStream


class _Stream:

    """Internal abstraction for storing history of a stream of strings.

    This class is basically a wrapper around a list, with extra metadata
    to allow reasoning about read/write access. It also supports iterating
    the content and waiting for new messages if the writer has yet to
    finish streaming.

    """

    def __init__(self, name: str, write_name: str):
        """Instantiate a new stream.

        Takes a name for read access and a name for write access.

        Args:
            name (str): The resource name to use for read access
                to the stream.
            write_name (str): The resource name to use for write
                access to the stream.

        """
        self._logger = logging.getLogger(__name__)
        self.name = name
        self.write_name = write_name

        self._content: List[bytes] = []
        self._write_event = Event()
        self._committed = False
        self._reader_count = 0
        self._reader_events: Queue = Queue()
        self._reader_lock = Lock()

    def __len__(self) -> int:
        return len(self._content)

    def write(self, message: bytes) -> None:
        """Append a message to the stream.

        Take a string and append it to the stream's content. Notify
        any readers that a new message has been appended.

        Args:
            message (bytes): The message to append to the stream.

        """
        if not self._committed:
            self._content.append(message)
            self._write_event.set()
            self._write_event.clear()

    def _get_next_message(self, offset: Optional[int]=None) -> Tuple[int, bytes]:
        """Return the next message after a given offset, waiting if needed.

        Take an integer index into the content list marking the offset
        to start reading from, and return the next index and message.

        If there are no further messages already in the stream, wait
        for notification of a call to write before returning the last
        element in the stream, and its index.

        Args:
            offset (int): The index to get the message from. If unset
                (or ``None``), this defaults to the beginning of the
                stream.

        """
        # No offset given means start at the beginning.
        if offset is None:
            offset = 0

        while not self._committed:
            if offset < len(self._content):
                return offset + 1, self._content[offset]

            # Wait for a new message to be written, then return it and its
            # index in the content list.
            new_content = self._write_event.wait(timeout=30)
            if new_content:
                return len(self._content), self._content[-1]

        # If the stream is committed, return the final message and offset
        return offset, self._content[offset]

    def get_content(self) -> Iterator[ReadResponse]:
        """Iterate the content of the stream until the writer finishes.

        When the current content of the stream is exhausted but the writer
        hasn't committed the stream, this method blocks to wait for more
        content.

        """
        offset = None
        with self._reader_lock:
            self._reader_count += 1
            self._reader_events.put(True)
        while not self._committed:
            next_offset, message = self._get_next_message(offset=offset)
            # If the returned offset is unchanged, it means we committed
            # the stream after we started waiting for messages
            if next_offset != offset:
                yield ReadResponse(data=message)
            offset = next_offset

    def to_logstream(self) -> LogStream:
        """Return a LogStream message for this stream."""
        return LogStream(
            name=self.name,
            write_resource_name=self.write_name
        )

    def commit(self) -> None:
        """Commit a stream, disallowing further writes."""
        self._committed = True

    def remove_reader(self) -> None:
        """Decrement the count of readers for this stream."""
        with self._reader_lock:
            self._reader_count -= 1

    def has_readers(self) -> bool:
        return self._reader_count > 0

    def wait_for_reader(self, timeout=None) -> bool:
        try:
            return self._reader_events.get(timeout=timeout)
        except Empty:
            return False

    @property
    def completed(self) -> bool:
        """Whether a stream is done with or not.

        A stream is considered completed if it has been committed by
        a writer, and has no remaining readers.

        """
        return self._committed and self._reader_count == 0


class StreamStorage:

    """Class to store a collection of in-memory text streams.

    This provides methods to retrieve a previously-added stream using
    the resource name for either read or write access, as well as a way
    to obtain a ``LogStream`` message for a stream with a given key.

    """

    def __init__(self):
        """Instantiate a new stream store."""
        self._logger = logging.getLogger(__name__)
        self._streams: Dict[str, _Stream] = {}

    def add_stream(self, key: str, read_name: str, write_name: str) -> None:
        """Add a new stream to the store.

        Takes a key and read/write resource names, and creates a new
        ``_Stream`` object with the given key.

        Args:
            key (str): The key to associate with the new stream.
            read_name (str): The resource name to use for read access to
                the new stream.
            write_name (str): The resource name to use for write access
                to the new stream.

        """
        if key in self._streams:
            raise StreamAlreadyExistsError(
                f'A LogStream with the parent [{key}] already exists, '
                'and should be used instead of creating a new stream.')
        self._streams[key] = _Stream(read_name, write_name)

    def get_stream(self, read_name: str) -> Optional[_Stream]:
        """Return a stream by its read resource name.

        Takes a resource name and returns the ``_Stream`` object for a
        stream with the given name as its resource name for read access.

        If no such stream exists, then this method returns None.

        Args:
            read_name (str): The read resource name of the stream to
                be returned.

        """
        key = read_name.split('/logStreams/', 1)[0]
        stream = self._streams.get(key)
        if stream and stream.name != read_name:
            stream = None
        return stream

    def get_writeable_stream(self, write_name: str) -> Optional[_Stream]:
        """Return a stream by its write resource name.

        Takes a resource name and returns the ``_Stream`` object for a
        stream with the given name as its resource name for write access.

        If no such stream exists, then this method returns None.

        Args:
            write_name (str): The write resource name of the stream to
                be returned.

        """
        key = write_name.split('/logStreams/', 1)[0]
        stream = self._streams.get(key)
        if stream and stream.write_name != write_name:
            stream = None
        return stream

    def get_logstream(self, key: str) -> Optional[LogStream]:
        """Returns a ``LogStream`` gRPC object for a specified stream.

        Takes a key which maps to a ``_Stream``, and returns a ``LogStream``
        gRPC message object for that stream.

        If there is no string associated with the given key, then this
        method returns None.

        Args:
            key (str): The key used to store the stream to return
                a ``LogStream`` message for.

        """
        stream = self._streams.get(key)
        if stream is None:
            return None
        return stream.to_logstream()

    def finish_stream(self, stream: _Stream) -> None:
        """Commit the stream, and remove it from the store if its unneeded.

        A stream is unneeded/completed if it is committed and has no readers
        currently connected.

        Args:
            stream (_Stream): The stream object to commit and potentially
                clean up.

        """
        stream.commit()
        if stream.completed:
            key = stream.name.split('/logStreams/', 1)[0]
            self._logger.debug(f"Stream [{stream.name}] will be removed "
                               "since it is committed and has no readers.")
            self._streams.pop(key)

    def remove_reader(self, resource_name: str) -> None:
        """Remove a reader from a stream, and remove the stream if needed.

        To keep from indefinitely growing our memory footprint, committed
        streams with no readers can be deleted.

        Args:
            resource_name (str): The resource name for the stream to
                remove a reader from.

        """
        key = resource_name.split('/logStreams/', 1)[0]
        stream = self._streams.get(key)
        if not stream or stream.name != resource_name:
            raise NotFoundError(
                f"No stream with a resource name [{resource_name}].")

        stream.remove_reader()
        if stream.completed:
            self._logger.debug(f"Stream [{resource_name}] will be removed "
                               "since it is committed and has no readers.")
            self._streams.pop(key)
