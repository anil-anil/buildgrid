# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from typing import TYPE_CHECKING, List, Optional
import uuid

from buildgrid._enums import MetricRecordDomain
from buildgrid._exceptions import NotFoundError, PermissionDeniedError
from buildgrid._protos.google.bytestream.bytestream_pb2 import (
    ReadResponse,
    WriteResponse,
    WriteRequest
)
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import LogStream
from buildgrid.server.cas.instance import ByteStreamInstance
from buildgrid.server.logstream.streaming.memory import StreamStorage
from buildgrid.server.metrics_names import LOGSTREAM_WRITE_UPLOADED_BYTES_COUNT
from buildgrid.server.metrics_utils import Counter
if TYPE_CHECKING:
    from buildgrid.server.server import Server


class LogStreamInstance:

    def __init__(self, prefix: str, storage: Optional[StreamStorage]=None):
        self._logger = logging.getLogger(__name__)
        self.instance_name: Optional[str] = None
        self._prefix = prefix
        if storage is None:
            storage = StreamStorage()
        self._stream_store = storage

    def register_instance_with_server(self, instance_name: str, server: 'Server') -> None:
        if self.instance_name is None:
            server.add_logstream_instance(self, instance_name)
            self.instance_name = instance_name
        else:
            raise AssertionError("Instance already registered")

    def create_logstream(self, parent: str) -> LogStream:
        response = self._stream_store.get_logstream(parent)
        if response is None:
            response = LogStream()
            response.name = f"{parent}/logStreams/{self._prefix}{str(uuid.uuid4())}"
            response.write_resource_name = f"{response.name}/{str(uuid.uuid4())}"
            self._stream_store.add_stream(
                parent, response.name, response.write_resource_name)
        return response
