# Debian Buster base image with python 3.6:
#
image: python:3.6-buster

stages:
  - test
  - post
  - deploy


# Templates and global variables.
#
variables:
  BGD: "${CI_PROJECT_DIR}/env/bin/bgd"
  PYTHON: "${CI_PROJECT_DIR}/env/bin/python"
  TOOLS: "${CI_PROJECT_DIR}/env/bin"

.venv-template:
  before_script: &install-base
    - python3 -m venv env  # Creates a virtual environment
  cache:
    paths:
      - .pip


# Test stage, build and test the code.
#
unit-tests:
  stage: test
  extends: .venv-template
  script:
    - apt-get update && apt-get install -y --no-install-recommends postgresql sudo
    - useradd -ms /bin/bash tester && chown -R tester .pip env
    - sudo -u tester ${PYTHON} -m pip --cache-dir=.pip install  ".[auth,tests]" && sudo -u tester ${PYTHON} setup.py test
  after_script:
    - mkdir -p coverage/
    - cp .coverage coverage/coverage."${CI_JOB_NAME}"
  variables:
    PYTEST_ADDOPTS: "--color=yes"
  artifacts:
    paths:
      - coverage/

# Test with multiple interpreters on master or tagged release.
#
unit-tests-multiple-interpreters:
  image: python
  stage: test
  script:
    - apt-get update && apt-get install -y --no-install-recommends postgresql python-pip sudo libpython3.7-dev
    - pip install tox
    - useradd -ms /bin/bash tester
    - sudo -u tester tox
  only:
    - master
    - tags

type-check:
  stage: test
  extends: .venv-template
  script:
    - ${PYTHON} -m pip --cache-dir=.pip install ".[dev]"
    - ${PYTHON} -m mypy buildgrid/

dummy-tests:
  stage: test
  extends: .venv-template
  script:
    - ${PYTHON} -m pip --cache-dir=.pip install  "."
    - export LC_ALL=C.UTF-8
    - export LANG=C.UTF-8
    - ${BGD} server start data/config/default.conf &
    - sleep 5  # Allows server to boot
    - ${BGD} bot dummy &
    - ${BGD} cas upload-dummy
    - ${BGD} execute request-dummy --wait-for-completion

e2e-tests:
  stage: test
  image: registry.gitlab.com/buildgrid/buildbox/buildbox-e2e:latest
  script:
    - BUILDGRID_SOURCE_ROOT=`pwd` end-to-end-test.sh

cleanup-compose-test:
  image:
    name: docker/compose:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: test
  variables:
      DOCKER_HOST: tcp://docker:2375
  services:
    - docker:dind
  before_script:
    - cd tests/stress-testing-dockerfiles/compose-files/
    - docker-compose -v
    - docker volume prune --force
  script:
    - docker-compose -f docker-compose-cleanup.yml build --pull
    - docker-compose -f docker-compose-cleanup.yml up --abort-on-container-exit --scale cleanup=5
  after_script:
    - cd tests/stress-testing-dockerfiles/compose-files/
    - docker-compose -f docker-compose-cleanup.yml down --volume


# Post-build stage, documentation, coverage report...
#
documentation:
  stage: test
  interruptible: true
  extends: .venv-template
  script:
    - ${PYTHON} -m pip --cache-dir=.pip install  ".[docs]"
    - apt-get update  && apt-get install -y --no-install-recommends graphviz
    - ${PYTHON} setup.py build_sphinx
  after_script:
    - mkdir -p documentation/
    - cp -a docs/build/html/. documentation/
  artifacts:
    paths:
      - documentation/

coverage:
  stage: post
  extends: .venv-template
  dependencies:
    - unit-tests
  coverage: '/TOTAL +\d+ +\d+ +(\d+\.\d+)%/'
  script:
    - ${PYTHON} -m pip --cache-dir=.pip install coverage
    - cd coverage/ && ls -l .
    - ${PYTHON} -m coverage combine --rcfile=../.coveragerc --append coverage.*
    - ${PYTHON} -m coverage html --rcfile=../.coveragerc --directory .
    - ${PYTHON} -m coverage report --rcfile=../.coveragerc --show-missing
  artifacts:
    paths:
      - coverage/

# Deployment stage, only for merges which land on master branch.
#
pages:
  stage: deploy
  dependencies:
    - coverage
    - documentation
  script:
    - mkdir -p public/coverage/
    - cp -a coverage/* public/coverage/
    - ls -la public/coverage/
    - cp -a documentation/* public/
    - ls -la public/
  artifacts:
    paths:
      - public/
  only:
    - master

docker-image:
  image: docker:stable
  stage: deploy
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  before_script:
    - >
      docker login
      --username gitlab-ci-token
      --password ${CI_JOB_TOKEN}
      ${CI_REGISTRY}
    - docker info
  script:
    - >
      docker build
      --file Dockerfile --pull
      --tag ${CI_REGISTRY_IMAGE}/buildgrid:${CI_COMMIT_SHORT_SHA}
      .
  after_script:
    - >
      docker push
      ${CI_REGISTRY_IMAGE}/buildgrid:${CI_COMMIT_SHORT_SHA}
  only:
    - master

triggers:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends curl
  script:
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.com/api/v4/projects/buildgrid%2Fbuildbox%2Fbuildbox-e2e/trigger/pipeline
  variables:
    GIT_STRATEGY: none
  only:
    - master
