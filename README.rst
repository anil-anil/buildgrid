.. _about:

About
=====


.. _what-is-it:

What is BuildGrid?
------------------

BuildGrid is a Python remote execution service which implements Google's
`Remote Execution API`_ and the `Remote Workers API`_. The project's goal is to
be able to execute build jobs remotely on a grid of computers in order to
massively speed up build times. Workers on the grid should be able to run with
different environments. It works with clients such as `Bazel`_,
`BuildStream`_ and `RECC`_, and is designed to be able to work with any client
that conforms to the above API protocols.

BuildGrid is designed to work with any worker conforming to the `Remote Workers API`_
specification. Workers actually execute the jobs on the backend while BuildGrid does
the scheduling and storage. The `BuildBox`_ ecosystem provides a suite of workers and
sandboxing tools that work with the Workers API and can be used with BuildGrid.

.. _Remote Execution API: https://github.com/bazelbuild/remote-apis
.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU/edit#heading=h.1u2taqr2h940
.. _BuildStream: https://wiki.gnome.org/Projects/BuildStream
.. _Bazel: https://bazel.build
.. _RECC: https://gitlab.com/bloomberg/recc
.. _BuildBox: https://buildgrid.gitlab.io/buildbox/buildbox-home/


.. _whats-going-on:

What's Going On?
----------------

Recently we finished implementing basic CAS expiry for the S3 storage backend,
when used alongside an SQL CAS index. This uses a new ``bgd cleanup`` command,
use of which is described `in the documentation`_.

.. _in the documentation: https://buildgrid.build/user/using_cas_cleanup.html

"Permissive Bot Session mode" has been added to the Bots service, which
makes it possible for bots to reconnect transparently after a server restart
with an UpdateBotSession request rather than that causing an error and loss
of in-progress leases. However, bots must still connect to the same server
instance every request, otherwise the BotSession reaper may terminate leases
that are actually progressing without issue.

A number of new metrics have been added in the last few months too, allowing
monitoring of the state of CAS and the ActionCache.

We've also made the validation of the YAML configuration file more strict, to
provide more useful error messages when something is wrong. A side-effect of
this is that now all keys in the file must use ``-`` as the word separator.
Previously some keys (notably the keys used with custom YAML tags) allowed
``_`` to be used interchangably.

Next we're working on adding support for streaming logs from workers back to
clients as Actions are being executed.

See our `release notes`_ for the latest changes/updates.

.. _release notes: https://gitlab.com/BuildGrid/buildgrid/-/blob/master/release_notes.rst

.. _readme-getting-started:

Getting started
---------------

Please refer to the `documentation`_ for `installation`_ and `usage`_
instructions, plus guidelines for `contributing`_ to the project.

.. _contributing: https://buildgrid.build/developer/contributing.html
.. _documentation: https://buildgrid.build/
.. _installation: https://buildgrid.build/user/installation.html
.. _usage: https://buildgrid.build/user/using.html


.. _about-resources:

Resources
---------

- `Homepage`_
- `GitLab repository`_
- `Bug tracking`_
- `Mailing list`_
- `Slack channel`_ [`invite link`_]
- `FAQ`_

.. _Homepage: https://buildgrid.build/
.. _GitLab repository: https://gitlab.com/BuildGrid/buildgrid
.. _Bug tracking: https://gitlab.com/BuildGrid/buildgrid/boards
.. _Mailing list: https://lists.buildgrid.build/cgi-bin/mailman/listinfo/buildgrid
.. _Slack channel: https://buildteamworld.slack.com/messages/CC9MKC203
.. _invite link: https://bit.ly/2SG1amT
.. _FAQ: https://buildgrid.build/user/faq.html
