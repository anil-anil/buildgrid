#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --no-emit-trusted-host --no-index requirements.auth.txt
#
cffi==1.14.0              # via -r requirements.auth.txt, cryptography
cryptography==2.9.2       # via -r requirements.auth.txt
pycparser==2.20           # via -r requirements.auth.txt, cffi
pyjwt==1.7.1              # via -r requirements.auth.txt
six==1.14.0               # via -r requirements.auth.txt, cryptography
